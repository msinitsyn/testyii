<?php

use yii\db\Migration;

/**
 * Class m171204_084810_update_project_table
 */
class m171204_084810_update_project_table extends Migration
{
    /**
     * @inheritdoc
     */
    /*public function safeUp()
    {

    }*/

    /**
     * @inheritdoc
     */
    /*public function safeDown()
    {
        echo "m171204_084810_update_project_table cannot be reverted.\n";

        return false;
    }*/


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $adm_id = \app\models\User::find()->where(['username' => 'admin'])->one()->id;

        if(isset($adm_id)){
            $this->alterColumn('project', 'user_id', 'integer NOT NULL DEFAULT ' . $adm_id);
        }

    }

    public function down()
    {
        echo "m171204_084810_update_project_table cannot be reverted.\n";

        return false;
    }

}
