<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m171203_065825_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'name' => $this->string(),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string()->defaultValue(NULL),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
